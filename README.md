# Mythic Table

**Welcome to Mythic Table!<br/>A virtual tabletop application for playing games with your friends online.<br/>If this is your first time here, read more on our website: [https://mythictable.com/](https://mythictable.com/)**

## Quick Start 

If the following doesn't make sense, please contact someone in our [Slack](mythictable.slack.com). We'll help you out the best we can! <br/>Many who find this page first will be software developers so we made some assumptions.

1. [Install .NET Core SDK, version 3.0 or higher](https://dotnet.microsoft.com/download) (backend)
2. Install node.js using Visual Studio Installer or [nodejs.org](https://nodejs.org/en/download/) (frontend)
3. [Install Powershell](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-windows-powershell?view=powershell-6) (startup script) *Don't want to install Powershell? See [here](##Non-Powershell-startup)*
4. Open a Powershell window
5. Clone the mythicauth repository (`git clone https://gitlab.com/mythicteam/mythicauth.git`)
6. Run `dotnet run` from the `mythicauth\src\MythicAuth` folder to launch the auth service
7. Open another Powershell window
8. Clone the main repository (`git clone https://gitlab.com/mythicteam/mythictable.git`)
9. Run `Start-DevEnv.ps1 -isFirstTime` from the main `mythictable` folder to launch the main application
10. Enjoy!
