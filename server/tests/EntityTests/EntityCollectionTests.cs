using System;
using Newtonsoft.Json;
using Xunit;
using MythicTable.GameSession;

namespace EntityTests
{
    public class EntityCollectionTests
    {
        [Fact]
        public void EntityCollectionCanStoreParsedDynamicJson()
        {
            var collection = new EntityCollection();

            dynamic json = JsonConvert.DeserializeObject(@"{""id"":""foobar"" }");

            collection.Add(json);
        }

        [Fact]
        public void EntityCollectionThrowsOnAddingJsonWithoutId()
        {
            var collection = new EntityCollection();

            dynamic json = JsonConvert.DeserializeObject(@"{""value"":true }");

            Assert.Throws<ArgumentException>(() =>
            {
                collection.Add(json);
            });
        }

        [Theory]
        [InlineData("1")]
        [InlineData("-1000")]
        [InlineData("0.1234")]
        [InlineData("\"\"")]
        public void EntityCollectionAcceptsVariousIdTypes(string idJsonString)
        {
            var collection = new EntityCollection();

            dynamic json = JsonConvert.DeserializeObject($@"{{""id"": {idJsonString} }}");

            collection.Add(json);
        }

        [Theory]
        [InlineData("{}")]
        [InlineData("[]")]
        [InlineData("null")]
        [InlineData("true")]
        [InlineData("false")]
        public void EntityCollectionThrowsOnAddingJsonWithNonStringCoercibleId(string idJsonString)
        {
            var collection = new EntityCollection();

            dynamic json = JsonConvert.DeserializeObject($@"{{""id"": {idJsonString} }}");

            Assert.Throws<ArgumentException>(() =>
            {
                collection.Add(json);
            });
        }
    }
}
