﻿using Newtonsoft.Json;

namespace MythicTable.GameSession
{
    public class DiceRoll
    {
        [JsonProperty("timestamp")]
        public double Timestamp { get; set; }

        [JsonProperty("userId")]
        public string UserId { get; set; }

        [JsonProperty("sessionId")]
        public string SessionId { get; set; }

        [JsonProperty("formula")]
        public string Formula { get; set; }

        public string Result { get; set; }
        public string ClientId { get; set; }
    }
}
