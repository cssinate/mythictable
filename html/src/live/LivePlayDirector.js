// Use require() instead of import as SignalR does not use ES6-style export
const signalR = require('@microsoft/signalr');

class LivePlayDirector {
    constructor(store) {
        this.store = store;
        this.connection = null;
        store.commit('live/setDirector', this);
    }

    get sessionId() {
        return this.state.sessionId;
    }
    set sessionId(id) {
        this.state.sessionId = id;
    }

    get state() {
        return this.store.state.live;
    }

    async init() {
        console.log(this.store.state.oidcStore.user.given_name);
        this.store.commit('live/setUserId', this.store.state.oidcStore.user.given_name);

        let accessToken = this.store.state.oidcStore.access_token;
        this.connection = new signalR.HubConnectionBuilder()
            .withUrl('/api/live', { accessTokenFactory: () => accessToken })
            .withAutomaticReconnect()
            .build();

        this.connection.on('ConfirmOpDelta', this.onConfirmDelta.bind(this));
        this.connection.on('ExceptionRaised', this.onExceptionRaised.bind(this));
        this.connection.on('ReceiveDiceResult', this.onReceiveDiceResult.bind(this));
        this.connection.on('Undo', this.undo.bind(this));
        this.connection.on('Redo', this.redo.bind(this));
        this.connection.onclose(() => {
            // FIXME: PoC only; needs to be mutation if used in prod
            this.store.state.live.connected = false;
        });
    }

    async connect() {
        this.state.connected = this.connection.start();
        try {
            await this.state.connected;
        } catch (e) {
            this.state.connected = false;
            throw e;
        }
        this.state.connected = true; // FIXME: PoC only; needs to be mutation if used in prod
        await this.initializeEntities();
    }

    async initializeEntities() {
        const resp = await fetch(`/api/s/${this.sessionId}/entities`);
        if (resp.status != 200) {
            return;
        }

        const entities = await resp.json();
        for (let entity of entities) {
            this.store.dispatch('gamestate/entities/update', {
                id: entity.id,
                value: entity,
            });
        }
        this.store.dispatch('gamestate/setBase');
    }

    onConfirmDelta(sessionDelta) {
        this.store.dispatch('gamestate/applyDelta', sessionDelta.delta);
        // When a new delta is applied, a new timeline is created so all undone deltas are reset.
        this.store.commit('gamestate/resetUndoneDeltas');
    }

    onExceptionRaised(error) {
        console.err('Exception raised by server: ' + error);
    }

    // the submitted delta should be an array of jsonpatches, i.e. an array of arrays of jsonpatch operations
    submitDelta(submittedDelta) {
        // If submitted delta is just a single JSONpatch operation, wrap it in the structure
        if (!Array.isArray(submittedDelta)) submittedDelta = [[submittedDelta]];
        // Check if every value of the array is an array with objects in it.
        submittedDelta.forEach((jsonpatch, idx) => {
            let wrappedJsonpatch = !Array.isArray(jsonpatch) ? [jsonpatch] : jsonpatch;

            wrappedJsonpatch.forEach(operation => {
                if (typeof operation !== 'object') throw new TypeError('JSONPatch of submitted delta is malformed');
            });
            submittedDelta[idx] = wrappedJsonpatch;
        });
        this.tryDelta({ delta: submittedDelta });
    }
    async tryDelta(delta) {
        await this.connection.invoke('submitDeltaTemp', delta);
    }

    submitUndo() {
        let undoObject = 'undo placeholder';
        this.tryUndo(undoObject);
    }
    async tryUndo(undoObject) {
        await this.connection.invoke('submitUndo', undoObject);
    }
    undo() {
        this.store.dispatch('gamestate/undo');
    }

    submitRedo() {
        let redoObject = 'redo placeholder';
        this.tryRedo(redoObject);
    }
    async tryRedo(redoObject) {
        await this.connection.invoke('submitRedo', redoObject);
    }
    redo() {
        this.store.dispatch('gamestate/redo');
    }

    moveToken(id, newPos) {
        const patch = [{ op: 'replace', path: '/token/pos', value: newPos }];

        this.tryStep({ entities: [{ id, patch }] });
    }

    async tryStep(step) {
        await this.connection.invoke('submitDelta', step);
    }

    submitRoll(diceObject) {
        this.tryRollDice(diceObject);
    }
    async tryRollDice(diceObject) {
        //TODO Check for valid diceObject
        await this.connection.invoke('rollDice', diceObject);
    }
    onReceiveDiceResult(diceResult) {
        let patch = { op: 'add', path: '/global/rollLog/-', value: diceResult };
        this.store.dispatch('gamestate/applyDelta', patch);
    }
}

export { LivePlayDirector as default };
