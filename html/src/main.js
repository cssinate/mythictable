﻿import Vue from 'vue';
import router from './router.js';
import store from './store.js';
import VueGtag from 'vue-gtag';

import GameStateStore from './store/GameStateStore';
import actions from './ruleset/experiment/actions';

// FIXME: find a better place to load in the ruleset than main.js
const ruleset = {
    actions: actions,
};
GameStateStore.state.ruleset = ruleset;

Vue.use(VueGtag, {
    config: { id: process.env.VUE_APP_ANALYTICS },
    appName: 'Mythic Table',
    pageTrackerScreenviewEnabled: true,
});

// eslint-disable-next-line no-unused-vars
const app = new Vue({
    el: '#app',
    store,
    router,
    render: h => h('router-view'),
});

// export { store };
