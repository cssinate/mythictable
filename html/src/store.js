import Vue from 'vue';
import Vuex from 'vuex';
import { vuexOidcCreateStoreModule } from 'vuex-oidc';

import GameStateStore from './store/GameStateStore';
import { oidcSettings } from './oidc';
import LivePlayState from './live/LivePlayState';

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        live: LivePlayState,
        gamestate: GameStateStore,
        oidcStore: vuexOidcCreateStoreModule(
            oidcSettings,
            {
                dispatchEventsOnWindow: true,
            },
            {
                // remove in prod
                userLoaded: user => console.log('OIDC user is loaded:', user),
                userUnloaded: () => console.log('OIDC user is unloaded'),
                accessTokenExpiring: () => console.log('Access token will expire'),
                accessTokenExpired: () => console.log('Access token did expire'),
                silentRenewError: () => console.log('OIDC user is unloaded'),
                userSignedOut: () => console.log('OIDC user is signed out'),
                oidcError: payload => console.log(`An error occured at ${payload.context}:`, payload.error),
            },
        ),
    },
});

export default store;
