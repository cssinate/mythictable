import Vue from 'vue';
import VueRouter from 'vue-router';
import store from './store.js';
import { vuexOidcCreateRouterMiddleware } from 'vuex-oidc';

Vue.use(VueRouter);

const routes = [
    {
        path: '*',
        component: () => import('./views/CampaignManagement'),
        name: 'manage-campaigns',
    },
    {
        path: '/play/:sessionId/:sceneId',
        component: () => import('./views/LivePlay'),
        props: true,
    },
    {
        path: '/play',
        name: 'live-play',
        redirect: '/play/$demo/stronghold',
    },
    {
        path: '/$debug/*',
        component: () => import('./debug/DebugPage.vue'),
    },
    {
        path: '/$debug',
        redirect: '/$debug/',
    },
    {
        path: '/home',
        component: () => import('./Home.vue'),
        meta: { isPublic: true },
    },
    {
        path: '/oidc',
        name: 'oidcCallback',
        component: () => import('./OidcCallback.vue'),
    },
];

const router = new VueRouter({
    routes,
    mode: 'history',
});
router.beforeEach(vuexOidcCreateRouterMiddleware(store));

export default router;
