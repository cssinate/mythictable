const entities = [
    {
        id: 'marc',
        token: {
            image: { asset: '.', width: 300, height: 300, origin: { x: 150, y: 150 } },
            scene: 'stronghold',
            pos: { q: 7, r: 18 },
        },
        asset: { kind: 'image', src: '/static/assets/marc.png' },
    },
    {
        id: 'sarah',
        token: {
            image: { asset: '.', width: 300, height: 300, origin: { x: 150, y: 150 } },
            scene: 'stronghold',
            pos: { q: 8, r: 18 },
        },
        asset: { kind: 'image', src: '/static/assets/sarah.png' },
    },
    {
        id: 'mirko',
        token: {
            image: { asset: '.', width: 300, height: 300, origin: { x: 150, y: 150 } },
            scene: 'stronghold',
            pos: { q: 7, r: 19 },
        },
        asset: { kind: 'image', src: '/static/assets/mirko.png' },
    },
    {
        id: 'jon',
        token: {
            image: { asset: '.', width: 300, height: 300, origin: { x: 150, y: 150 } },
            scene: 'stronghold',
            pos: { q: 8, r: 19 },
        },
        asset: { kind: 'image', src: '/static/assets/jon.png' },
    },
    {
        id: 'redcap01',
        token: {
            image: { asset: '.', width: 300, height: 300, origin: { x: 150, y: 150 } },
            scene: 'stronghold',
            pos: { q: 31, r: 0 },
        },
        asset: { kind: 'image', src: '/static/assets/Redcap.png' },
    },
    {
        id: 'redcap02',
        token: {
            image: { asset: '.', width: 300, height: 300, origin: { x: 150, y: 150 } },
            scene: 'stronghold',
            pos: { q: 31, r: 0 },
        },
        asset: { kind: 'image', src: '/static/assets/Redcap.png' },
    },
    {
        id: 'redcap03',
        token: {
            image: { asset: '.', width: 300, height: 300, origin: { x: 150, y: 150 } },
            scene: 'stronghold',
            pos: { q: 31, r: 0 },
        },
        asset: { kind: 'image', src: '/static/assets/Redcap.png' },
    },
    {
        id: 'wolf01',
        token: {
            image: { asset: '.', width: 300, height: 300, origin: { x: 150, y: 150 } },
            scene: 'stronghold',
            pos: { q: 31, r: 1 },
        },
        asset: { kind: 'image', src: '/static/assets/Wolf.png' },
    },
    {
        id: 'wolf02',
        token: {
            image: { asset: '.', width: 300, height: 300, origin: { x: 150, y: 150 } },
            scene: 'stronghold',
            pos: { q: 31, r: 1 },
        },
        asset: { kind: 'image', src: '/static/assets/Wolf.png' },
    },
    {
        id: 'wolf03',
        token: {
            image: { asset: '.', width: 300, height: 300, origin: { x: 150, y: 150 } },
            scene: 'stronghold',
            pos: { q: 31, r: 1 },
        },
        asset: { kind: 'image', src: '/static/assets/Wolf.png' },
    },
    {
        id: 'red01',
        token: {
            image: { color: 'red' },
            scene: 'stronghold',
            pos: { q: 31, r: 2 },
        },
    },
    {
        id: 'red02',
        token: {
            image: { color: 'red' },
            scene: 'stronghold',
            pos: { q: 31, r: 2 },
        },
    },
    {
        id: 'red03',
        token: {
            image: { color: 'red' },
            scene: 'stronghold',
            pos: { q: 31, r: 2 },
        },
    },
    {
        id: 'red04',
        token: {
            image: { color: 'red' },
            scene: 'stronghold',
            pos: { q: 31, r: 2 },
        },
    },
    {
        id: 'black01',
        token: {
            image: { color: 'black' },
            scene: 'stronghold',
            pos: { q: 31, r: 3 },
        },
    },
    {
        id: 'black02',
        token: {
            image: { color: 'black' },
            scene: 'stronghold',
            pos: { q: 31, r: 3 },
        },
    },
    {
        id: 'black03',
        token: {
            image: { color: 'black' },
            scene: 'stronghold',
            pos: { q: 31, r: 3 },
        },
    },
    {
        id: 'black04',
        token: {
            image: { color: 'black' },
            scene: 'stronghold',
            pos: { q: 31, r: 3 },
        },
    },
    {
        id: 'black05',
        token: {
            image: { color: 'black' },
            scene: 'stronghold',
            pos: { q: 31, r: 3 },
        },
    },
    {
        id: 'black06',
        token: {
            image: { color: 'black' },
            scene: 'stronghold',
            pos: { q: 31, r: 3 },
        },
    },
    {
        id: 'black07',
        token: {
            image: { color: 'black' },
            scene: 'stronghold',
            pos: { q: 31, r: 3 },
        },
    },
    {
        id: 'black08',
        token: {
            image: { color: 'black' },
            scene: 'stronghold',
            pos: { q: 31, r: 3 },
        },
    },
    {
        id: 'black09',
        token: {
            image: { color: 'black' },
            scene: 'stronghold',
            pos: { q: 31, r: 3 },
        },
    },
    {
        id: 'black10',
        token: {
            image: { color: 'black' },
            scene: 'stronghold',
            pos: { q: 31, r: 3 },
        },
    },
    {
        id: 'blue01',
        token: {
            image: { color: 'blue' },
            scene: 'stronghold',
            pos: { q: 31, r: 4 },
        },
    },
    {
        id: 'blue02',
        token: {
            image: { color: 'blue' },
            scene: 'stronghold',
            pos: { q: 31, r: 4 },
        },
    },
    {
        id: 'blue03',
        token: {
            image: { color: 'blue' },
            scene: 'stronghold',
            pos: { q: 31, r: 4 },
        },
    },
    {
        id: 'blue04',
        token: {
            image: { color: 'blue' },
            scene: 'stronghold',
            pos: { q: 31, r: 4 },
        },
    },
    {
        id: 'plusButton',
        asset: { kind: 'image', src: '/static/assets/buttons/Buttons-plus.jpg' },
    },
    {
        id: 'minusButton',
        asset: { kind: 'image', src: '/static/assets/buttons/Buttons-minus.jpg' },
    },
    {
        id: 'undoButton',
        asset: { kind: 'image', src: '/static/assets/buttons/Buttons-undo.jpg' },
    },
    {
        id: 'redoButton',
        asset: { kind: 'image', src: '/static/assets/buttons/Buttons-redo.jpg' },
    },

    // Stages/scenes ----------------------------
    {
        id: 'stronghold',
        scene: { stage: '.' },
        stage: {
            grid: { type: 'square', size: 50 },
            bounds: {
                nw: { q: -1, r: -1 },
                se: { q: 29, r: 20 },
            },
            color: '#223344',
            elements: [
                {
                    id: 'background',
                    asset: 'stronghold_bg',
                    pos: { q: 0, r: 0, pa: '00' },
                },
            ],
        },
    },

    // Plain assets --------------------------
    {
        id: 'stronghold_bg',
        asset: { kind: 'image', src: '/static/assets/hideout.png' },
    },
];

const loader = async store => {
    await store.dispatch('gamestate/entities/load', entities);
};

export { loader, entities };
