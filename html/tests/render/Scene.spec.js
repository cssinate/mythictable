import Vuex from 'vuex';
import { shallowMount, createLocalVue } from '@vue/test-utils';

import Scene from '@/render/Scene.vue';
import GameStateStore from '@/store/GameStateStore';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        gamestate: GameStateStore,
    },
});

describe('Scene component', () => {
    const wrapper = shallowMount(Scene, { localVue, store });
    const vm = wrapper.vm;

    describe('defaults', () => {
        describe('.renderContext', () => {
            const context = vm.renderContext;

            it('is defined', () => {
                expect(context).toBeDefined();
            });

            it('has gridSpace property', () => {
                expect(context.gridSpace).toBeDefined();
            });

            // Skipping for the #67 change
            it.skip('has default gridSpace matching configs from stage', () => {
                expect(context.gridSpace.type).toEqual(vm.stage.grid.type);
                expect(context.gridSpace.size).toEqual(vm.stage.grid.size);
            });

            it('has pixelRatio', () => {
                expect(vm.renderContext.pixelRatio).toEqual(1);
            });
        });

        describe('.stage', () => {
            const stage = vm.stage;

            describe('has default stage...', () => {
                it('size is 50x50', () => {
                    expect(stage.bounds).toEqual({
                        nw: { q: 0, r: 0 },
                        se: { q: 49, r: 49 },
                    });
                });

                it('with square grid of size 50', () => {
                    expect(stage.grid.type).toEqual('square');
                    expect(stage.grid.size).toEqual(50);
                });
            });
        });
    });

    describe('with non-default stage', () => {
        const wrapper = shallowMount(Scene, { localVue, store });
        const vm = wrapper.vm;
        const gridSize = 10;

        vm.stage.grid = { size: gridSize };

        // Skipping this because of a hack  to resize the maps for the stream
        test.skip('gridSpace size matches stage after map is changed', () => {
            expect(vm.renderContext.gridSpace.size).toEqual(gridSize);
        });
    });
});
